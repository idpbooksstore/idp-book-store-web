import React from 'react';
import { Button, Container, Heading } from '@chakra-ui/react';
import { Book } from '../lib/book';
import BookBody from './BookBody';

interface BookViewProps {
  onClose: () => void;
  addToCart: (item: Book, amount: number) => void;
  book: Book;
}

const BookView: React.FC<BookViewProps> = ({ book, onClose, addToCart }) => {
  const buyClickedHandler = () => {
    onClose();
    addToCart(book, 1);
  };

  return (
    <Container>
      <Heading>
        {book.title} {book.author}
      </Heading>
      <>
        <BookBody price={book.price} reviews={book.reviews} genre={book.genre} />
      </>
      <>
        <Button onClick={onClose}>Close</Button>
        <Button onClick={buyClickedHandler}>Buy</Button>
      </>
    </Container>
  );
};

export default BookView;
