import { Text, VStack } from '@chakra-ui/layout';
import React from 'react';

interface Props {
  title: string;
  author: string;
}

const BookPreview = ({ title, author }: Props) => {
  return (
    <VStack>
      <Text fontSize="5xl">{title}</Text>
      <Text fontSize="2xl">{author}</Text>
    </VStack>
  );
};

export default BookPreview;
