import { ListItem, Stat, StatHelpText, StatLabel, StatNumber, Tag, UnorderedList } from '@chakra-ui/react';
import React from 'react';
import { Review } from '../lib/book';
import BookReview from './BookReview';

type Props = {
  reviews: Review[];
  price: number;
  genre: string;
};

const BookBody = ({ reviews, price, genre }: Props) => {
  return (
    <>
      <Stat>
        <StatLabel>Price</StatLabel>
        <StatNumber>{price} RON</StatNumber>
        <StatHelpText>
          <Tag>{genre}</Tag>
        </StatHelpText>
      </Stat>
      <UnorderedList>
        {reviews?.map((review, index) => (
          <ListItem key={index}>
            <BookReview {...review} />
          </ListItem>
        ))}
      </UnorderedList>
    </>
  );
};

export default BookBody;
