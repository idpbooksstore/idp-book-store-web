import { Box, HStack, Stat, Text, StatLabel, StatNumber } from '@chakra-ui/react';
import React from 'react';

interface Props {
  score: number;
  message: string;
}

const BookReview = ({ score, message }: Props) => {
  return (
    <HStack>
      <Box>
        <Stat>
          <StatLabel>Score</StatLabel>
          <StatNumber>{score}/5</StatNumber>
        </Stat>
      </Box>
      <Box>
        <Text>{message}</Text>
      </Box>
    </HStack>
  );
};

export default BookReview;
