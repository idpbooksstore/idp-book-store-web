import React from 'react';
import {
  Button,
  FormLabel,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { useQuery } from 'react-query';
import { deleteCartRequest, getCartRequest } from '../lib/cart';
import CartPreview from './CartPreview';
import { Field, Form, Formik, FormikHelpers } from 'formik';
import { postCheckoutRequest } from '../lib/checkout';

interface CartPanelProps {}

interface Values {
  card: string;
  cvc: string;
  exp: string;
  address: string;
}

const useCart = () => useQuery(['cart'], () => getCartRequest(), { refetchInterval: 1000 });

const CartPanel: React.FC<CartPanelProps> = ({}) => {
  const { status, data } = useCart();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const buyClickedHandler = () => onOpen();
  const cancelClickedHandler = () => deleteCartRequest();

  switch (status) {
    case 'loading':
      return <Text>Loading...</Text>;
    case 'error':
      return <Text>Error</Text>;
    default:
      return (
        <>
          <VStack align="flex-start">
            <CartPreview items={data!.items} />
            <Button onClick={buyClickedHandler}>Buy</Button>
            <Button onClick={cancelClickedHandler}>Cancel</Button>
          </VStack>

          <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>Checkout</ModalHeader>
              <ModalCloseButton />
              <Formik
                initialValues={{
                  card: '4242424242424242',
                  cvc: '1234',
                  exp: '02/2022',
                  address: 'address',
                }}
                onSubmit={({ address, ...card }: Values, { setSubmitting }: FormikHelpers<Values>) => {
                  onClose();
                  setSubmitting(false);
                  return postCheckoutRequest({ address, card });
                }}
              >
                <Form>
                  <ModalBody>
                    <FormLabel htmlFor="card">Card</FormLabel>
                    <Field id="card" name="card" placeholder="4242424242424242" />

                    <FormLabel htmlFor="cvc">CVC</FormLabel>
                    <Field id="cvc" name="cvc" placeholder="1234" />

                    <FormLabel htmlFor="exp">Expiration Date</FormLabel>
                    <Field id="exp" name="exp" placeholder="02/2022" />

                    <FormLabel htmlFor="address">Address</FormLabel>
                    <Field id="address" name="address" placeholder="address" />
                  </ModalBody>

                  <ModalFooter>
                    <Button colorScheme="blue" mr={3} type="submit">
                      Confirm
                    </Button>
                    <Button variant="ghost" onClick={onClose}>
                      Close
                    </Button>
                  </ModalFooter>
                </Form>
              </Formik>
            </ModalContent>
          </Modal>
        </>
      );
  }
};

export default CartPanel;
