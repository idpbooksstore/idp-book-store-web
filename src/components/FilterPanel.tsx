import { VStack } from '@chakra-ui/layout';
import { Button, Text } from '@chakra-ui/react';
import React, { useState } from 'react';
import { useQuery } from 'react-query';
import { FilterDict, getFiltersRequest } from '../lib/filter';
import { KeyOf } from '../lib/general';
import StringFilterContainer from './StringFilterContainer';

interface FilterPanelProps {
  initialFilters: FilterDict;
  onFilterApply: (filters: FilterDict) => void;
}

const useFilters = () => useQuery(['filters'], () => getFiltersRequest());

const FilterPanel: React.FC<FilterPanelProps> = ({ initialFilters, onFilterApply }) => {
  const [activeFilters, setActiveFilters] = useState<FilterDict>(initialFilters);
  const { status, data } = useFilters();

  const stringFilterChangedHandler = (filterName: KeyOf<FilterDict>) => (newValues: string[]) =>
    setActiveFilters({ ...activeFilters, [filterName]: newValues });

  const searchClickedHandler = () => onFilterApply(activeFilters);

  switch (status) {
    case 'loading':
      return <Text>Loading...</Text>;
    case 'error':
      return <Text>Error</Text>;
    default:
      return (
        <VStack align="flex-start">
          <StringFilterContainer
            filterName="Authors"
            filters={data!.filters.author}
            initialFilters={activeFilters.author}
            onFilterUpdate={stringFilterChangedHandler('author')}
          />
          <StringFilterContainer
            filterName="Genres"
            filters={data!.filters.genre}
            initialFilters={activeFilters.genre}
            onFilterUpdate={stringFilterChangedHandler('genre')}
          />
          <Button onClick={searchClickedHandler}>Search</Button>
        </VStack>
      );
  }
};

export default FilterPanel;
