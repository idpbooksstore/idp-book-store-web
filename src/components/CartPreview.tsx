import { Box, Stat, StatHelpText, StatLabel, StatNumber, Text, VStack } from '@chakra-ui/react';
import React from 'react';
import { useQuery } from 'react-query';
import { getBooksByIdRequest } from '../lib/book';
import { ItemData } from '../lib/cart';
import BookPreview from './BookPreview';

interface CartPreviewProps {
  items: ItemData[];
}

const useBooksById = (id: string[]) => useQuery(['booksById', id], () => getBooksByIdRequest(id));

const CartPreview: React.FC<CartPreviewProps> = ({ items }) => {
  const { status, data } = useBooksById(items.map((item) => item._id));

  switch (status) {
    case 'loading':
      return <Text>Loading...</Text>;
    case 'error':
      return <Text>Error</Text>;
    default:
      return (
        <VStack>
          {data!.books.map((book, index) => (
            <Box key={index}>
              <BookPreview title={book.title} author={book.author} />
              <Stat>
                <StatLabel>Total</StatLabel>
                <StatNumber>{items[index]!.amount * book.price}</StatNumber>
                <StatHelpText>Amount: {items[index]!.amount}</StatHelpText>
              </Stat>
            </Box>
          ))}
        </VStack>
      );
  }
};

export default CartPreview;
