import { Box, Checkbox, Text, VStack } from '@chakra-ui/react';
import React, { useState } from 'react';

interface StringFilterContainerProps {
  filterName: string;
  filters: string[];
  initialFilters: string[];
  onFilterUpdate: (filters: string[]) => void;
}

const StringFilterContainer: React.FC<StringFilterContainerProps> = ({
  filterName,
  filters,
  initialFilters,
  onFilterUpdate,
}) => {
  const [activeFilters, setActiveFilters] = useState<string[]>(initialFilters);

  const changedHandler = (checked: boolean, filter: string) => {
    const newActiveFilters = checked ? [...activeFilters, filter] : activeFilters.filter((f) => f !== filter);
    setActiveFilters(newActiveFilters);
    onFilterUpdate(newActiveFilters);
  };

  return (
    <Box>
      <Text fontSize="2xl">{filterName}</Text>
      <VStack>
        {filters.map((filter, index) => (
          <Checkbox
            key={index}
            spacing="1rem"
            defaultChecked={activeFilters.includes(filter)}
            onChange={(event) => changedHandler(event.target.checked, filter)}
          >
            {filter}
          </Checkbox>
        ))}
      </VStack>
    </Box>
  );
};

export default StringFilterContainer;
