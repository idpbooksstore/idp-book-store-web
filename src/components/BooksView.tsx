import { SimpleGrid } from '@chakra-ui/layout';
import { Box, Text } from '@chakra-ui/react';
import React, { useState } from 'react';
import { useQuery } from 'react-query';
import { Book, getBooksRequest } from '../lib/book';
import { FilterDict } from '../lib/filter';
import BookPreview from './BookPreview';
import BookView from './BookView';

interface BooksViewProps {
  filters: FilterDict;
  addToCart: (book: Book, amount: number) => void;
}

const useBooks = (filters: FilterDict) => useQuery(['books', filters], () => getBooksRequest(filters));

const BooksView: React.FC<BooksViewProps> = ({ filters, addToCart }) => {
  const [openedBook, setOpenedBook] = useState<Book | undefined>();
  const { status, data } = useBooks(filters);

  const onOpen = (book: Book) => setOpenedBook(book);
  const onClose = () => setOpenedBook(undefined);

  switch (status) {
    case 'loading':
      return <Text>Loading...</Text>;
    case 'error':
      return <Text>Error</Text>;
    default:
      return openedBook === undefined ? (
        <SimpleGrid minChildWidth="120px" spacing="40px">
          {data!.books.map((book, index) => (
            <Box key={index} onClick={() => onOpen(book)}>
              <BookPreview title={book.title} author={book.author} />
            </Box>
          ))}
        </SimpleGrid>
      ) : (
        <BookView book={openedBook} onClose={onClose} addToCart={addToCart} />
      );
  }
};

export default BooksView;
