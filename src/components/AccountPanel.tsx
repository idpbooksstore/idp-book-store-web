import { Button, FormLabel, VStack } from '@chakra-ui/react';
import { Field, Form, Formik, FormikHelpers } from 'formik';
import React from 'react';
import { postAuthLoginRequest, postAuthRegisterRequest } from '../lib/auth';

interface AccountPanelProps {}

interface Values {
  username: string;
  password: string;
  type: 'register' | 'login';
}

const AccountPanel: React.FC<AccountPanelProps> = ({}) => {
  return (
    <Formik
      initialValues={{
        username: '',
        password: '',
        type: 'register',
      }}
      onSubmit={({ type, ...authData }: Values, { setSubmitting }: FormikHelpers<Values>) => {
        setSubmitting(false);
        if (type === 'register') return postAuthRegisterRequest(authData);
        if (type === 'login') return postAuthLoginRequest(authData);
        return undefined;
      }}
    >
      {({ setFieldValue }) => (
        <Form>
          <VStack align="flex-start">
            <FormLabel htmlFor="username">Username</FormLabel>
            <Field id="username" name="username" placeholder="Username" />

            <FormLabel htmlFor="password">Password</FormLabel>
            <Field id="password" name="password" placeholder="Password" type="password" />

            <Button
              id="register"
              onClick={() => {
                setFieldValue('type', 'register');
              }}
              colorScheme="teal"
              type="submit"
            >
              Register
            </Button>
            <Button
              id="login"
              onClick={() => {
                setFieldValue('type', 'login');
              }}
              colorScheme="teal"
              type="submit"
            >
              Login
            </Button>
          </VStack>
        </Form>
      )}
    </Formik>
  );
};

export default AccountPanel;
