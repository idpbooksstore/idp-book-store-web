import { Box, Flex, Tab, TabList, TabPanel, TabPanels, Tabs, useToast } from '@chakra-ui/react';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import { useRouter } from 'next/dist/client/router';
import React from 'react';
import AccountPanel from '../components/AccountPanel';
import BooksView from '../components/BooksView';
import CartPanel from '../components/CartPanel';
import FilterPanel from '../components/FilterPanel';
import { Book } from '../lib/book';
import { postCartAddRequest } from '../lib/cart';
import { FilterDict } from '../lib/filter';
import { parseQuery } from '../lib/general';

interface Props {
  initialFilters: FilterDict;
}

export const getServerSideProps: GetServerSideProps<Props> = async ({ query }) => {
  const initialFilters = parseQuery(query);

  return {
    props: { initialFilters },
  };
};

const Index = ({ initialFilters }: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();
  const toast = useToast();

  const filtersAppliedHandler = (newActiveFilters: FilterDict) => {
    router.push({ href: '/', query: { ...newActiveFilters } });
  };

  const addToCart = (book: Book, amount: number) => {
    postCartAddRequest({ _id: book._id, amount });
    return toast({
      title: `${book.title}`,
      description: `Added to cart`,
      status: 'success',
      duration: 5000,
      isClosable: true,
    });
  };

  return (
    <Flex>
      <Box>
        <Tabs>
          <TabList>
            <Tab>Filters</Tab>
            <Tab>Cart</Tab>
            <Tab>Account</Tab>
          </TabList>

          <TabPanels>
            <TabPanel>
              <FilterPanel initialFilters={initialFilters} onFilterApply={filtersAppliedHandler} />
            </TabPanel>
            <TabPanel>
              <CartPanel />
            </TabPanel>
            <TabPanel>
              <AccountPanel />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
      <Box flex="1">
        <BooksView filters={initialFilters} addToCart={addToCart} />
      </Box>
    </Flex>
  );
};

export default Index;
