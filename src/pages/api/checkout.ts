import axios from 'axios';
import { NextApiResponse, NextApiRequest } from 'next';
import { env } from '../../lib/env';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { status, data } = await axios({
      url: `http://${env.api.url}/api/checkout`,
      method: 'POST',
      data: req.body,
    });
    res.status(status).json({ message: data.message });
  } catch (error) {
    res.status(500).json({ message: error });
  }
};
