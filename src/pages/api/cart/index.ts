import axios from 'axios';
import { NextApiResponse, NextApiRequest } from 'next';
import { env } from '../../../lib/env';

export default async (_: NextApiRequest, res: NextApiResponse) => {
  try {
    const { status, data } = await axios({
      url: `http://${env.api.url}/api/cart`,
      method: 'GET',
    });
    res.status(status).json({ items: data.items, message: data.message });
  } catch (error) {
    res.status(500).json({ items: [], message: error });
  }
};
