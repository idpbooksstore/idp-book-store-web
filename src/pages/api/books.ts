import axios from 'axios';
import { NextApiResponse, NextApiRequest } from 'next';
import { env } from '../../lib/env';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { status, data } = await axios({
      url: `http://${env.api.url}/api/books`,
      params: req.query,
      method: 'GET',
    });
    res.status(status).json({ books: data.books, message: data.message });
  } catch (error) {
    res.status(500).json({ books: [], message: error });
  }
};
