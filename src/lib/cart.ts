import axios from 'axios';

export interface ItemData {
  _id: string;
  amount: number;
}

interface GetCartRequest {
  status: number;
  message: any;
  items: ItemData[];
}

export const getCartRequest = async (): Promise<GetCartRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/cart`,
      method: 'GET',
    });
    return { status, items: data.items, message: data.message };
  } catch (error) {
    return { status: 500, items: [], message: error };
  }
};

interface PostCartAddRequest {
  status: number;
  message: any;
  items: ItemData[];
}

export const postCartAddRequest = async (item: ItemData): Promise<PostCartAddRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/cart/add`,
      method: 'POST',
      data: { item },
    });
    return { status, items: data.items, message: data.message };
  } catch (error) {
    return { status: 500, items: [], message: error };
  }
};

interface PostCartRemoveRequest {
  status: number;
  message: any;
  items: ItemData[];
}

export const postCartRemoveRequest = async (item: ItemData): Promise<PostCartRemoveRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/cart/rm`,
      method: 'POST',
      data: { item },
    });
    return { status, items: data.items, message: data.message };
  } catch (error) {
    return { status: 500, items: [], message: error };
  }
};

interface DeleteCartRequest {
  status: number;
  message: any;
  items: ItemData[];
}

export const deleteCartRequest = async (): Promise<DeleteCartRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/cart/clear`,
      method: 'DELETE',
    });
    return { status, items: data.items, message: data.message };
  } catch (error) {
    return { status: 500, items: [], message: error };
  }
};
