import axios from 'axios';

export interface AuthData {
  username: string;
  password: string;
}

interface PostAuthRegisterRequest {
  status: number;
  message: any;
}

export const postAuthRegisterRequest = async (authData: AuthData): Promise<PostAuthRegisterRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/auth/register`,
      method: 'POST',
      data: authData,
    });
    return { status, message: data.message };
  } catch (error) {
    return { status: 500, message: error };
  }
};

interface PostAuthLoginRequest {
  status: number;
  message: any;
}

export const postAuthLoginRequest = async (authData: AuthData): Promise<PostAuthLoginRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/auth/login`,
      method: 'POST',
      data: authData,
    });
    return { status, message: data.message };
  } catch (error) {
    return { status: 500, message: error };
  }
};
