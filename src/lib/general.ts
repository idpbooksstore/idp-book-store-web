import { ParsedUrlQuery } from 'querystring';
import { FilterDict } from './filter';

export type KeyOf<T> = keyof T;

export type ValueOf<T> = T[keyof T];

export type ArrayElement<ArrayType extends readonly unknown[]> = ArrayType extends readonly (infer ElementType)[]
  ? ElementType
  : never;

const getQueryField = (query: ParsedUrlQuery, key: KeyOf<FilterDict>) => query[key];

const parseStringFilterQuery = (value: string | string[] | undefined) => {
  if (typeof value === 'string') return [value];
  if (Array.isArray(value)) return value;
  return [];
};

export const parseQuery = (query: ParsedUrlQuery): FilterDict => {
  return {
    author: parseStringFilterQuery(getQueryField(query, 'author')),
    genre: parseStringFilterQuery(getQueryField(query, 'genre')),
  };
};
