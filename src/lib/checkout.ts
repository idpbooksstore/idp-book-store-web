import axios from 'axios';

export interface CheckoutData {
  address: string;
  card: {
    card: string;
    cvc: string;
    exp: string;
  };
}

interface PostCheckoutRequest {
  status: number;
  message: any;
}

export const postCheckoutRequest = async (checkoutData: CheckoutData): Promise<PostCheckoutRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/checkout`,
      method: 'POST',
      data: checkoutData,
    });
    return { status, message: data.message };
  } catch (error) {
    return { status: 500, message: error };
  }
};
