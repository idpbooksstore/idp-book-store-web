import axios from 'axios';

type StringFilter = string[];

export interface FilterDict {
  author: StringFilter;
  genre: StringFilter;
}

interface GetFiltersRequest {
  status: number;
  message: string;
  filters: FilterDict;
}

export const getFiltersRequest = async (): Promise<GetFiltersRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/filters`,
      method: 'GET',
    });
    return { status, filters: data.filters, message: data.message };
  } catch (error) {
    return { status: 500, filters: { author: [], genre: [] }, message: error };
  }
};
