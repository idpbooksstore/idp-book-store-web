import axios from 'axios';
import { FilterDict } from './filter';

export interface Book {
  _id: string;
  title: string;
  author: string;
  genre: string;
  reviews: Review[];
  price: number;
}

export interface Review {
  score: number;
  message: string;
}

interface GetBooksRequest {
  status: number;
  message: string;
  books: Book[];
}

export const getBooksRequest = async (filters: FilterDict): Promise<GetBooksRequest> => {
  try {
    const { status, data } = await axios({
      url: `/api/books`,
      params: filters,
      method: 'GET',
    });
    return { status, books: data.books, message: data.message };
  } catch (error) {
    return { status: 500, books: [], message: error };
  }
};

interface GetBooksByIdRequest {
  status: number;
  message: string;
  books: Book[];
}

export const getBooksByIdRequest = async (id: string[]): Promise<GetBooksByIdRequest> => {
  try {
    if (id.length === 0) return { status: 200, books: [], message: 'Empty cart' };
    const { status, data } = await axios({
      url: `/api/books`,
      params: { id },
      method: 'GET',
    });
    return { status, books: data.books, message: data.message };
  } catch (error) {
    return { status: 500, books: [], message: error };
  }
};
