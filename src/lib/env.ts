export const env = {
  api: {
    url: process.env['NEXT_PUBLIC_API_URL'] ?? 'localhost',
  },
  auth: {
    token: process.env['NEXT_PUBLIC_AUTH_TOKEN'] ?? 'authtoken',
  },
  nodeEnv: process.env.NODE_ENV ?? 'development',
};
