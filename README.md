# Online Book Store - Frontend

This project represents an extension of [Online Book Store API](https://gitlab.com/idpbooksstore/idp-book-store-api) and it is used for testing basic REST API functionality.

## Getting started

##### Tested with:
* Windows 10
* NodeJS v15.11.0
* npm 7.6.0

##### Requirements:
In order to run this application you will need NodeJS and npm or yarn installed.
* [NodeJS](https://nodejs.org/en/download/)
* [npm](https://www.npmjs.com/get-npm)
* [yarn](https://classic.yarnpkg.com/en/docs/install/)

##### Usage
1. Download the repository.
```shell
$ git clone https://gitlab.com/idpbooksstore/idp-book-store-web.git
$ cd idp-book-store-web
```
2. Create an `.env.local` file and use the example file [./.env.example](./.env.example) to set the used environment variables .
3. Install the dependencies.
```bash
$ npm ci
```
4. Start the application in development mode.
```bash
$ npm run dev
```
6. Or you can build the application and then run the compiled version.
```bash
$ npm run build
$ npm run start
```
5. The application will run on `localhost:3000`.


##### Environment variables
* NEXT_PUBLICAPI_URL= Represents the URL address of the [server](https://gitlab.com/idpbooksstore/idp-book-store-api). For example it can be `localhost:8000` if the server runs on the local machine. The default value is `localhost:8000`.
* NODE_ENV= Represents a NodeJS specific variable that is used to decide if the application runs in production or development. It can be `production` or `development`. The default value is `development`.
